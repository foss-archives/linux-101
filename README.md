# Linux 101

## History

Linux is an open source Unix-like operating system based on the Linux kernel. The first operating system kernel
was released in 1991 by Linus Torvalds. Linux is packaged in a Linux distribution, nowadays we have several distributions
including **Debian**, **Fedora**, **Ubuntu**, **Redhat**, and **SUSE Linux**. 

## Essential Commands

We have compiled a list of essential commands required when working on a Linux system. We will
go over these commands in further detail.

- `cat`: display content of file  
- `cd`: change file permissions
- `chmod`: change user 
- `cp`: copy file or directories
- `echo`:  display string to standard output
- `find`: search for files in directory
- `less`: similar to `more` but you can move up and down
- `ls`: show content of files in directory
- `man`:  show man pages for any linux command
- `more`: stream a file one page at a time
- `mv`: move file or direectory
- `pwd:` display current working directory
- `rm`: remove files or directories
- `rmdir`: remove empty directories
- `which` - show location of command

## Getting Started

You can follow along this exercise on a laptop if you have a Linux machine, if not you can download 
[docker](https://docs.docker.com/get-docker/) and spin up an interactive session in ubuntu container as follows:

```shell
$ docker run -it ubuntu
root@7e6cbf64493c:/# 
``` 

Before you start please run the following to update all packages available packages provided by base operating system.

```shell
root@7e6cbf64493c:/# apt update
```

## Navigating through Filesystem

Let's navigate through the Linux filesystem. The `pwd` will show location of 
working directory. The root of filesystem is `/` in all Linux distribution.

```shell
root@7e6cbf64493c:/# pwd  
/
```

If you run `cd` without any argument it will change to your HOME directory. 

```shell
root@7e6cbf64493c:/# cd
root@7e6cbf64493c:~# pwd
/root
```

In Linux parent directory can be accessed via `..` and current directory is referenced as `.` 
so if you want to go up a directory you can do following. 

```shell 
root@f5fe1e2ad705:~# cd ..
root@f5fe1e2ad705:/# pwd
/
root@f5fe1e2ad705:/# cd .
root@f5fe1e2ad705:/# pwd
/
```

You can access HOME directory in one of the following ways all of them are valid

```
cd
cd $HOME
cd ~
```

To see content of your current directory, use `ls` command. The `-la` are options to `ls` command
which tweak behavior of ls command. 

```shell
root@7e6cbf64493c:~# cd
root@7e6cbf64493c:~# ls -la
total 16
drwx------ 2 root root 4096 Oct  6 16:58 .
drwxr-xr-x 1 root root 4096 Nov 11 19:29 ..
-rw-r--r-- 1 root root 3106 Dec  5  2019 .bashrc
-rw-r--r-- 1 root root  161 Dec  5  2019 .profile
```

Almost all Linux command comes with options, you can find more detail about any linux command
by running `--help` or `-h` option for any command or run `man <command>`.

To see man pages for `ls` you can run

```shell
man ls
```

We can see content of file via `cat` command and specify name of file. Let's take a look at content of `.profile`. Notice that
output was displayed to terminal, this is referred as `stdout`  

```shell
root@7e6cbf64493c:~# cat .profile 
# ~/.profile: executed by Bourne-compatible login shells.

if [ "$BASH" ]; then
  if [ -f ~/.bashrc ]; then
    . ~/.bashrc
  fi
fi

mesg n 2> /dev/null || true
```

Let's create a new directory named `files` via `mkdir`. We can navigate  to this directory
via `cd`. Note, our current directory is now `/root/files`

```shell
root@7e6cbf64493c:~# mkdir files
root@7e6cbf64493c:~# cd files/
root@7e6cbf64493c:~/files# pwd
/root/files
```

We can navigate to any directory in filesystem using absolute path. Note all directories start from 
root `/` and each subdirectory noted by backslash symbol (`/`). In this example we navigate to root, home
directory and `/root/files`.

```shell
root@7e6cbf64493c:~/files# cd /
root@7e6cbf64493c:/# pwd
/
root@7e6cbf64493c:~# cd /root/files
root@7e6cbf64493c:~/files# pwd
/root/files
```

Let's create an empty file via `touch` command and call it `names.txt`.  

```shell
root@7e6cbf64493c:~/files# touch names.txt
root@7e6cbf64493c:~/files# ls -l
total 0
-rw-r--r-- 1 root root 0 Nov 11 19:50 names.txt
```

Right now the content of this file is empty if we try `cat` this file we will
see the following

```shell
root@7e6cbf64493c:~/files# cat names.txt 
root@7e6cbf64493c:~/files# 
```

We can rename a file via `mv` command which can be used to rename file or directory or move
files between directories. For now we will rename file in same directory and call it `countries.txt`.
Notice that `names.txt` is no longer present in output of `ls`.

```shell
root@7e6cbf64493c:~/files# mv names.txt countries.txt
root@7e6cbf64493c:~/files# ls -l
total 0
-rw-r--r-- 1 root root 0 Nov 11 19:50 countries.txt
```

Let's create a few more files via `touch` command. We can specify file names as positional 
arguments to `touch` command as shown below. In this example we create three files `a.txt`, 
`b.txt`, `c.txt`

```shell
root@7e6cbf64493c:~/files# touch a.txt b.txt c.txt
root@7e6cbf64493c:~/files# ls -l
total 0
-rw-r--r-- 1 root root 0 Nov 11 20:01 a.txt
-rw-r--r-- 1 root root 0 Nov 11 20:01 b.txt
-rw-r--r-- 1 root root 0 Nov 11 20:01 c.txt
-rw-r--r-- 1 root root 0 Nov 11 19:50 countries.txt
```

We can remove a file via `rm`. In this example we will remove `a.txt`. 

```shell
root@7e6cbf64493c:~/files# rm a.txt 
root@7e6cbf64493c:~/files# ls -l
total 0
-rw-r--r-- 1 root root 0 Nov 11 20:01 b.txt
-rw-r--r-- 1 root root 0 Nov 11 20:01 c.txt
-rw-r--r-- 1 root root 0 Nov 11 19:50 countries.txt
```

The `rm` command can be used to remove directory and files. If there are files in a
directory then you will get this error. 

```shell
root@7e6cbf64493c:~/files# rm /root/files/
rm: cannot remove '/root/files/': Is a directory
```

This is because `/root/files` is a directory and `rm` wouldn't delete directory unless 
all files are removed. You can remove all files all files in current directory via

```
rm *
```

Similarly you can delete an entire directory via `rm -rf` option. 

```
rm -rf /root/files
```

Once you delete a directory it won't be accessible in filesystem. Let's run
the following commands:

```shell
root@7e6cbf64493c:~# rm -rf /root/files/
root@7e6cbf64493c:~# ls -l /root/files
ls: cannot access '/root/files': No such file or directory
```

Let's create a few more directories via `mkdir` in our $HOME directory (`/root`). Each
directory will be denoted by `d` as leading character in output of `ls -l`. We will cover
file [permission later](./index.md#file-permission).

```shell
root@7e6cbf64493c:~# mkdir names countries languages
root@7e6cbf64493c:~# ls -l
total 12
drwxr-xr-x 2 root root 4096 Nov 11 20:17 countries
drwxr-xr-x 2 root root 4096 Nov 11 20:17 languages
drwxr-xr-x 2 root root 4096 Nov 11 20:17 names
```

Next let's create some files in each directory as follows. The `{` and `}` symbol
can be used to create multiple files in a directory

```shell
root@7e6cbf64493c:~# touch countries/{usa,canada}.txt names/{richard,mike}.txt languages/{english,french,spanish}.txt
```

The `ls -lR` will show content of each file recursively as you can see below.

```shell
root@7e6cbf64493c:~# ls -lR
.:
total 12
drwxr-xr-x 2 root root 4096 Nov 11 20:22 countries
drwxr-xr-x 2 root root 4096 Nov 11 20:22 languages
drwxr-xr-x 2 root root 4096 Nov 11 20:22 names

./countries:
total 0
-rw-r--r-- 1 root root 0 Nov 11 20:22 canada.txt
-rw-r--r-- 1 root root 0 Nov 11 20:22 usa.txt

./languages:
total 0
-rw-r--r-- 1 root root 0 Nov 11 20:22 english.txt
-rw-r--r-- 1 root root 0 Nov 11 20:22 french.txt
-rw-r--r-- 1 root root 0 Nov 11 20:22 spanish.txt

./names:
total 0
-rw-r--r-- 1 root root 0 Nov 11 20:22 mike.txt
-rw-r--r-- 1 root root 0 Nov 11 20:22 richard.txt
```

If you want to create a bunch of files with a numeric name let's say `f0-f9.txt` we
can do this as follows. 

```shell
root@7e6cbf64493c:~# mkdir numbers
root@7e6cbf64493c:~# touch numbers/f{0..9}.txt
root@7e6cbf64493c:~# ls -l numbers/
total 0
-rw-r--r-- 1 root root 0 Nov 11 20:27 f0.txt
-rw-r--r-- 1 root root 0 Nov 11 20:27 f1.txt
-rw-r--r-- 1 root root 0 Nov 11 20:27 f2.txt
-rw-r--r-- 1 root root 0 Nov 11 20:27 f3.txt
-rw-r--r-- 1 root root 0 Nov 11 20:27 f4.txt
-rw-r--r-- 1 root root 0 Nov 11 20:27 f5.txt
-rw-r--r-- 1 root root 0 Nov 11 20:27 f6.txt
-rw-r--r-- 1 root root 0 Nov 11 20:27 f7.txt
-rw-r--r-- 1 root root 0 Nov 11 20:27 f8.txt
-rw-r--r-- 1 root root 0 Nov 11 20:27 f9.txt
```

We can display content to terminal via `echo` command.

```shell
root@7e6cbf64493c:~# echo "My name is Richard"
My name is Richard
```

Similarly, we can redirect output from stdout to file using the `>` symbol. Let's 
run this same command as follows:

```shell
root@7e6cbf64493c:~# echo "My name is Richard" > names/richard.txt 
root@7e6cbf64493c:~# 
```

Notice that output is not shown to screen because stdout was redirected to file `names/richard.txt`. 
Let's see if our echo statement is present in file. 

```shell
root@7e6cbf64493c:~# cat names/richard.txt 
My name is Richard
```

Let's try adding another line to file and see what happens. We see that our content
was overwritten, thats because `>` will overwrite content of file with output from command.

```shell
root@7e6cbf64493c:~# echo "My age is 35" > names/richard.txt
root@7e6cbf64493c:~# cat names/richard.txt 
My age is 35
``` 

In order to append content to file we need to use `>>`. As you can tell, we can erase content
of file by running the following:

```shell
root@7e6cbf64493c:~# > names/richard.txt 
root@7e6cbf64493c:~# cat names/richard.txt 
root@7e6cbf64493c:~# 
```

Let's run the following. Notice now we have two lines and second line `My age is 35` was 
appended to file.

```shell
root@7e6cbf64493c:~# echo "My name is Richard" > names/richard.txt
root@7e6cbf64493c:~# echo "My age is 35" >> names/richard.txt
root@7e6cbf64493c:~# cat names/richard.txt 
My name is Richard
My age is 35
```

## Input, Output and Error Streams

In Linux we have three streams `stdin`, `stdout` and `stderr` which are used for input, output and error stream. Input stream
can be input from keyboard or input from program or file. The output stream can display result to terminal or redirect to file. 
Error messages can be displayed to terminal or error file. 

The stdin, stdout and stderr streams are file descriptors that are referenced as `0`, `1`, and `2`. Note everything in
Linux is a file even these streams. These streams are located in `/dev/{stdin|stdout|stderr}`

```shell
root@f5fe1e2ad705:~# ls -l /dev/std*
lrwxrwxrwx 1 root root 15 Nov 11 23:19 /dev/stderr -> /proc/self/fd/2
lrwxrwxrwx 1 root root 15 Nov 11 23:19 /dev/stdin -> /proc/self/fd/0
lrwxrwxrwx 1 root root 15 Nov 11 23:19 /dev/stdout -> /proc/self/fd/1
```

stdout and standard error are both displayed to terminal but they 
have different file descriptors. We can see this from this example. 
Let's try accessing an invalid file directory. We can infer this looks like
an error message

```shell
root@7e6cbf64493c:~# ls -l /home/user
ls: cannot access '/home/user': No such file or directory
```

If we try redirecting stdout to file we see that message was print to screen because 
this message is coming from stderr.

```shell
root@f5fe1e2ad705:~# ls -l /home/user 1> /tmp/stdout.txt
ls: cannot access '/home/user': No such file or directory
root@f5fe1e2ad705:~# cat /tmp/stdout.txt 
root@f5fe1e2ad705:~# 
```

Therefore we need to redirect stderr to file we can do this by specifying `2>` 
the following. Now error stream was written to file.

```shell
root@f5fe1e2ad705:~# ls -l /home/user 2> /tmp/stderr.txt
root@f5fe1e2ad705:~# cat /tmp/stderr.txt 
ls: cannot access '/home/user': No such file or directory
```

A typical use-case can be to redirect output to file like `.out` and you can
redirect error to `.err` file. If you don't care about error message
you can send it to `/dev/null` which is a special stream that can be used to send
anything that is lost forever. 

```shell
root@7e6cbf64493c:~# ls /xyz 2>/dev/null
root@7e6cbf64493c:~# cat /dev/null 
root@7e6cbf64493c:~# 
```

## Piping output

You can pipe output from one command to another via `|` symbol which can be useful if you want to share
output from one command as input to another command. Let's first remove all files under `/root/files` directory

```shell
root@7e6cbf64493c:~# rm -rf /root/files
```

Take for instance let's take content of `~/.profile` and pipe to `tail` command which will show the last N lines from file. The 
`tail -n 1` will show last line of file. 

```shell
root@7e6cbf64493c:~# cat ~/.profile | tail -n 1
mesg n 2> /dev/null || true
```

Note this is equivalent to running

```shell
root@7e6cbf64493c:~# tail -n 1 ~/.profile 
mesg n 2> /dev/null || true
```

Piping can be done for multiple commands so long as output from previous command can be processed by subsequent command.

## File Permission

In Linux, file permission, attributes and ownership grant access level to files which are known as 
Access Control List (ACL). In Linux we have three file permissions **read** (`r`), **write** (`w`), 
**execute** (`x`) which are assigned to three sets `owner`, `group` and `other`. 

Shown below is an example file permission for `/root/.bashrc`

```shell
root@7e6cbf64493c:~# ls -l /root/.bashrc 
-rw-r--r-- 1 root root 3106 Dec  5  2019 /root/.bashrc
```

The first character indicates the type of file if you see `-` it means its a file type. 

The next set of three `rw-` refers to `read`, `write` to **owner**. A `-` indicates no permission 
in this case owner has no execute permission. 

The second set of three are **group** permission which are `r--` that means only `read` access to group and 

The third set of three permission are for **other** which is any user that logs in to system as `r--` permission which
is read only.
 
File permission are modified via `chmod`, they can be represented in octal, binary or character notation. The octal 
representation for each permission can be represented as follows

| permission | octal value |
|------------| -----------|
| `read`     |  4         |
| `write`    |  2         |
| `execute`  |  1         |

In example above the file permission are 644 which means owner has `6` (read+write) group and other have `4` which means
read only.

This allows us to represent all the permission by simply adding the permission we can get the following

 octal value | binary value | character notation | description  |
 ----------- | ------------ | -------------------| ----------- |
| 0           | 000         | `---`              | no permission (0) = 0                  | 
| 1           | 001         | `--x`              | execute (1) = 1                        |
| 2           | 010         | `-w-`              | write (2) = 2                          |
| 3           | 011         | `-wx`              | write (2) + execute (1) = 3            |
| 4           | 100         | `r--`              | read (4) = 4                           |
| 5           | 101         | `r-x`              | read (4) + execute (1) = 5             |
| 6           | 110         | `rw-`              | read (4) + write (2) = 6               |
| 7           | 111         | `rwx`              | read (4) + write (2) + execute (1) = 7 |

Let's navigate to our home directory and create a file named `permission.txt` 

```shell
root@7e6cbf64493c:~# cd 
root@7e6cbf64493c:~# touch permission.txt

root@7e6cbf64493c:~# ls -l permission.txt 
-rw-r--r-- 1 root root 0 Nov 11 22:14 permission.txt
```

We can update the permission using octal set for owner, group and other for now let's remove all permission by setting
permission to `000`.  Generally this is not advised and most likely never used since **owner** typically need access 
to read or write to file.

```shell
root@7e6cbf64493c:~# chmod 000 permission.txt 
root@7e6cbf64493c:~# ls -l permission.txt 
---------- 1 root root 0 Nov 11 22:14 permission.txt
```

We can grant owner full access (read, write, execute) via 700 permission. Recall that `4 + 2 + 1 = 7` is the read,
write and execute permission. 

```shell
root@7e6cbf64493c:~# ls -l permission.txt 
-rwx------ 1 root root 0 Nov 11 22:18 permission.txt
```

A typical file permission is `644` which means owner has read and write and group and other can read file. Let's try to set this
to following:

```shell
root@7e6cbf64493c:~# chmod 644 permission.txt 
root@7e6cbf64493c:~# ls -l permission.txt 
-rw-r--r-- 1 root root 0 Nov 11 22:18 permission.txt
```

We can update permission for `owner`, `group`, and `other` via character notation. Let's assume we want group to have `write`
access we can do the following. The `g+w` means add `w` permission for group 

```shell
root@7e6cbf64493c:~# chmod g+w permission.txt 
root@7e6cbf64493c:~# ls -l permission.txt 
-rw-rw-r-- 1 root root 0 Nov 11 22:18 permission.txt
```

Similarly, we can set permission using the `=` notation for a group set. Let's assume we want owner to have read, 
write and execute we can do the following:

```shell
root@7e6cbf64493c:~# chmod u=rwx permission.txt 
root@7e6cbf64493c:~# ls -l permission.txt 
-rwxrw-r-- 1 root root 0 Nov 11 22:18 permission.txt
```

We can remove a permission via `-` symbol for a group. The `o-r` will remove remov `read` permission for other as we can see below:

```shell
root@7e6cbf64493c:~# chmod o-r permission.txt 
-rwxrw-r-- 1 root root 0 Nov 11 22:18 permission.txt
root@7e6cbf64493c:~# ls -l permission.txt 
-rwxrw---- 1 root root 0 Nov 11 22:18 permission.txt
```

You can use `a` to apply permission to all users or `ugo` (user, group, other) for instance let's remove all permission and apply
read to everyone.

```shell
root@7e6cbf64493c:~# chmod 000 permission.txt 
root@7e6cbf64493c:~# ls -l permission.txt 
---------- 1 root root 0 Nov 11 22:18 permission.txt
root@7e6cbf64493c:~# chmod a+r permission.txt 
root@7e6cbf64493c:~# ls -l permission.txt 
-r--r--r-- 1 root root 0 Nov 11 22:18 permission.txt
```

In Linux, files and directories are created with default permission setting which is
configured based on **umask**. The default permission for files is `666` and directories is `777`. 
You can check the current umask by running `umask`. In our example below the umask is `0022`, the last 
three octal values `022` are applied on files.

```shell
root@f5fe1e2ad705:~# umask
0022
```

You can get permission for new files and directories by subtracting the umask from the default permission this
means:

- directory: `777` - `022` = `755`
- file: `666` - `022` = `644`

We can confirm this by creating a file and directory and check its permission. 

```shell
root@f5fe1e2ad705:~# mkdir new_dir
root@f5fe1e2ad705:~# touch new_file
root@f5fe1e2ad705:~# ls -ld new_dir new_file
drwxr-xr-x 2 root root 4096 Nov 12 17:00 new_dir
-rw-r--r-- 1 root root    0 Nov 12 17:01 new_file
```

You can learn more about unix permission and umask [here](../../filesystems/unix-file-permissions.md).

## What is PATH

The `PATH` is an environment variable responsible for finding binaries in your path without having to remember full path
to where binary is installed. This environment is a colon separate list of directories that are added to search path. 
The PATH environment has the following directories in path.

```shell
root@7e6cbf64493c:~# echo $PATH
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
```

This means when you run `cat` you are infact running binary installed in one
of the system path. You can find location of any binary via `which` command

```shell
root@7e6cbf64493c:~# which cat
/usr/bin/cat
```

One common mistake can be you override or erase `PATH` which can lead to interesting result. Take for instance
if we set `PATH` to empty string now we can't run any commands because there is nothing in search path

```shell
root@7e6cbf64493c:~# export PATH=''
root@7e6cbf64493c:~# ls
bash: ls: No such file or directory
```

In this situation we need to know full path to binary and this is not convenient for user so please never override `PATH` but always
append directories to current path.

Let's restore our PATH to its original value. Now we see all of our binaries are present

```shell
root@7e6cbf64493c:~# export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
root@7e6cbf64493c:~# ls
countries  error.txt  languages  names  numbers  permission.txt  user.txt
```

## Managing Processes 

You can see all active process via `top` command which shows a summary of number of process running on system
at a given time. A typical output will look something like this. You can use `top` to view, or kill
process. For help you can press `h` to see list of option. To quit simply run `q`. 

```shell
root@7e6cbf64493c:~# top

top - 23:14:02 up 13 days, 20:41,  0 users,  load average: 0.02, 0.04, 0.00
Tasks:   2 total,   1 running,   1 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.1 us,  0.3 sy,  0.0 ni, 99.6 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
MiB Mem :   1987.7 total,    113.0 free,    271.3 used,   1603.4 buff/cache
MiB Swap:   1024.0 total,    478.9 free,    545.1 used.   1526.3 avail Mem 

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND                                                                                                             
    1 root      20   0    4240   3620   3008 S   0.0   0.2   0:00.61 bash                                                                                                                
  193 root      20   0    6108   3232   2732 R   0.0   0.2   0:00.06 top   
```

When you run a process it is typically run in foreground which means it runs in your active shell
until process is complete. Take for instance this command which will sleep for 5 seconds and once process
is complete it will grant you control back to terminal.

```shell
root@7e6cbf64493c:~# sleep 5
root@7e6cbf64493c:~# 
```

You can run any process in background by specifying `&` as the last argument to command. Note you 
see that command runs in background and you have access to terminal. We can run `jobs` to see list of 
background jobs

```shell
root@f5fe1e2ad705:~# sleep 60 &
[1] 1414
root@f5fe1e2ad705:~# jobs
[1]+  Running                 sleep 60 &
```

You can kill any process using `kill`, the `%1` indicates kill the job 1 

```shell
root@f5fe1e2ad705:~# kill %1
root@f5fe1e2ad705:~# jobs
[1]+  Terminated              sleep 60
```

Each process has a process id (PID) which can be retrieved via `ps` command. If we run a sleep command we see
that it has a process id 1430. Note you may have a different process ID.

```shell
root@f5fe1e2ad705:~# sleep 360 &
[1] 1430
root@f5fe1e2ad705:~# ps
  PID TTY          TIME CMD
    1 pts/0    00:00:00 bash
 1430 pts/0    00:00:00 sleep
 1431 pts/0    00:00:00 ps
```

You can use `kill` command to specify process you want to terminate in this case let's terminate the kill process

```shell
root@f5fe1e2ad705:~# kill 1430
```

Let's run a few more sleep jobs in background. Each

```shell
root@f5fe1e2ad705:~# sleep 360 &
[1] 1458
root@f5fe1e2ad705:~# sleep 360 &
[2] 1459
```

We can see state of multiple process by specifying PID as positional argument to `ps` command

```shell
root@f5fe1e2ad705:~# ps 1458 1459
  PID TTY      STAT   TIME COMMAND
 1458 pts/0    S      0:00 sleep 360
 1459 pts/0    S      0:00 sleep 360
```

The `pkill` command is useful if you want to kill process based on pattern without remembering the process ID. For instance
we can kill both process via following command. Now both process are killed

```shell
root@f5fe1e2ad705:~# pkill -e sleep 
sleep killed (pid 1458)
sleep killed (pid 1459)
[1]-  Terminated              sleep 360
[2]+  Terminated              sleep 360
```

If you have a process in background you can use the `fg` command to bring it to foreground to see status of
process. For instance we can run the following

```shell
root@f5fe1e2ad705:~# time sleep 30 &
[1] 1468
root@f5fe1e2ad705:~# fg 1
time sleep 30

real	0m29.968s
user	0m0.001s
sys	0m0.001s
```

The `pstree` is a useful command to see content of process tree and relationship. This command won't be available
so you can run 

```shell
apt install psmisc
```

Let's run three more sleep jobs and run pstree against one of the process ID

```shell
root@f5fe1e2ad705:~# sleep 260 &
[1] 1523
root@f5fe1e2ad705:~# sleep 260 &
[2] 1530
root@f5fe1e2ad705:~# sleep 260 &
[3] 1531

root@f5fe1e2ad705:~# pstree 1531
sleep
```

Note we can find parent process that invoked this and show process ID via `-ps` option

```shell
root@f5fe1e2ad705:~# pstree -ps 1531
bash(1)---sleep(1531)
```

We can see all process associated to parent process `1`

```shell
root@f5fe1e2ad705:~# pstree -ps 1   
bash(1)-+-pstree(1535)
        |-sleep(1523)
        |-sleep(1530)
        `-sleep(1531)
```

## Command History

The `history` command is useful to keep track of previous commands run in your shell which can be useful
to troubleshoot what was run or be used to rerun commands. Take for instance output of history for the last 10 
commands

```shell
root@f5fe1e2ad705:~# history 10
  114  ls
  115  man ls
  116  pwd
  117  ls
  118  id 
  119  cat /etc/passwd 
  120  echo $SHELL
  121  env
  122  history
  123  history | tail -n 10
```

We can rerun any command using `!` followed by name of command identifier. Let's run command `116`, you can pick
whatever command you want to run

```shell
root@f5fe1e2ad705:~# !116
pwd
/root
```

We can use `!` followed by any linux command to run the last run command. For instance if you want to run the last 
`cat` command you can run the following

```shell
root@f5fe1e2ad705:~# !cat
cat ~/.profile 
# ~/.profile: executed by Bourne-compatible login shells.

if [ "$BASH" ]; then
  if [ -f ~/.bashrc ]; then
    . ~/.bashrc
  fi
fi

mesg n 2> /dev/null || true
```

You can set `HISTTIMEFORMAT` to format output of history command which is based on the
[date](https://man7.org/linux/man-pages/man1/date.1.html) format. For instance if we want
to retrieve full date and time we can set HISTTIMEFORMAT to the following

```
export HISTTIMEFORMAT="%F %T "
```

Now if we run history we will see output with date time stamp as shown below.

```shell
root@f5fe1e2ad705:~# history 2
  148  2021-11-12 01:39:30 HISTTIMEFORMAT="%F %T "
  149  2021-11-12 01:39:37 history 2
```

You can write your history file to disk via `history -w` if you run this command your history will
be stored in file `~/.bash_history`.

## Finding Files

The `find` command is a very useful command that can be used to find files that allows one to perform 
operation that you typically can't do with any other Linux command. 

You can run find on a directory and it will retrieve all files recursively.

```shell
root@f5fe1e2ad705:~# find $HOME
/root
/root/.profile
/root/.bashrc
/root/.config
/root/.config/procps
/root/.bash_history
```

Let's assume you want to file all directories in `/etc` and get a total count of files. This can be done 
as follows. The `-type d` will only retrieve files that are directory types

```shell
root@f5fe1e2ad705:~# find /etc -type d | wc -l
42
```

Let's try to find all files that start with `.bash` in HOME directory. We can use `-type f` to retrieve 
only file types and `-name` can be used to filter files based on expression

```shell
root@f5fe1e2ad705:~# find $HOME -type f -name ".bash*"
/root/.bashrc
/root/.bash_history
```

Find all character files using `-type c`. 

```shell
root@f5fe1e2ad705:~#  find / -type c -ls
       10      0 crw-rw-rw-   1 root     root       1,   9 Feb 22 20:33 /dev/urandom
        9      0 crw-rw-rw-   1 root     root       1,   5 Feb 22 20:33 /dev/zero
        8      0 crw-rw-rw-   1 root     root       5,   0 Feb 22 20:33 /dev/tty
        7      0 crw-rw-rw-   1 root     root       1,   7 Feb 22 20:33 /dev/full
        6      0 crw-rw-rw-   1 root     root       1,   8 Feb 22 20:33 /dev/random
        5      0 crw-rw-rw-   1 root     root       1,   3 Feb 22 20:33 /dev/null
        3      0 crw--w----   1 root     tty      136,   0 Feb 22 20:35 /dev/pts/0
        2      0 crw-rw-rw-   1 root     root       5,   2 Feb 22 20:35 /dev/pts/ptmx
```

Find all symbolic links in directory `/bin`

```shell
root@f5fe1e2ad705:~# find /bin -type l -ls
  1179654      0 lrwxrwxrwx   1 root     root            7 Oct  6 16:47 /bin -> usr/bin
```

We can find all files based based on a particular size which can be useful if you want to 
identify which files take up most space. This can be done via `-size` option as shown below

```shell
root@f5fe1e2ad705:~# find /var -size 10k       
/var/lib/dpkg/info/debconf.md5sums
```

We can run arbitrary commands against all files found using `-exec` option. Let's take this one step further
and see full size of this file using following command

```shell
root@f5fe1e2ad705:~# find /var -size 10k -exec ls -l {} \;
-rw-r--r-- 1 root root 10222 Aug  3  2019 /var/lib/dpkg/info/debconf.md5sums
```

You can search for all files owned by particular user by running by specifying `-user` and name 
of user that you want to check.

```shell
find /etc/ -user root
```

You can ignore case when searching for filename using `-iname` wheras `-name` will honor case. Let's
create two files named `a` and `A` in home directory. We see that running find with `-name` will honor case
while `-iname` will ignore case and retrieve both files.

```shell
root@f5fe1e2ad705:~# touch ~/a ~/A
root@f5fe1e2ad705:~# find $HOME -name a
/root/a
root@f5fe1e2ad705:~# find $HOME -iname a
/root/a
/root/A
```

We can find all files based on permission using the `-perm` option. Let's find all files that have `644` permission.
We can confirm with `-exec ls -l {} \;` that all of these files have the correct permission

```shell
root@f5fe1e2ad705:~# find .  -type f -perm 0644 -exec ls -l {} \;
-rw-r--r-- 1 root root 161 Dec  5  2019 ./.profile
-rw-r--r-- 1 root root 3106 Dec  5  2019 ./.bashrc
-rw-r--r-- 1 root root 0 Nov 12 02:06 ./a
-rw-r--r-- 1 root root 0 Nov 12 02:06 ./A
```

Alternatively you can find all files that don't have `644` permission as follows:

```shell
root@f5fe1e2ad705:~# find .  -type f ! -perm 0644 -exec ls -l {} \;
-rw------- 1 root root 44 Nov 12 01:40 ./.bash_history
```

You can find all empty files using `-empty` option

```shell
root@f5fe1e2ad705:~# find . -type f -empty
./a
./A
```

Likewise you can find all empty directories. Let's create a few directories before running find

```shell
root@f5fe1e2ad705:~# mkdir dir{1..6}
root@f5fe1e2ad705:~# find . -type d -empty       
./dir1
./dir6
./dir3
./dir2
./dir5
./.config
./dir4
```

Find all files modified 1 day ago.

```shell
root@f5fe1e2ad705:~# find /var -mtime 1 -exec ls -l {} \;
-rw-r--r-- 1 root root 1132589 Nov 10 10:03 /var/lib/apt/lists/security.ubuntu.com_ubuntu_dists_focal-security_restricted_binary-amd64_Packages.lz4
-rw-r--r-- 1 root root 50067 Nov 11 01:53 /var/lib/apt/lists/archive.ubuntu.com_ubuntu_dists_focal-updates_multiverse_binary-amd64_Packages.lz4
```

Find all files accessed 1 hour ago

```shell
root@f5fe1e2ad705:~# find $HOME -amin -60;
/root
/root/dir1
/root/dir6
/root/dir3
/root/dir2
/root/a
/root/dir5
/root/A
/root/.config
/root/.bash_history
/root/dir4
```

Delete all empty directories that start with name `dir*`. Notice before we delete files we have
6 directories and after deletion there 0 entries 

```shell
root@f5fe1e2ad705:~# find $HOME -empty -name "dir*" | wc -l
6
root@f5fe1e2ad705:~# find $HOME -empty -name "dir*" -delete
root@f5fe1e2ad705:~# find $HOME -empty -name "dir*" | wc -l
0
```

## Manual pages

The `man` command is extremely useful when you want to learn more about any particular command. 

Currently we don't have `man` in our docker instance

```shell
root@f5fe1e2ad705:~# man
bash: /usr/bin/man: No such file or directory
```

We can install this via `apt` as follows

```shell
apt install -y man
```

Now we have the `man` command. 

```shell
root@f5fe1e2ad705:~# which man
/usr/bin/man
```

**You may need to run `unminimize` which will restore content of ubuntu packages in order to make use or `man` command so you can see man pages for Linux command provided by Operating System.**


You can run man on `man` command to see what the command does.

```shell
man man
```

You can retrieve brief description for any given command using the `-f` option. In output
below we see `(1)` indicates there is on man page for `ls` command.

```shell
root@f5fe1e2ad705:~# man -f ls
ls (1)               - list directory contents
```

## Tab Completion

If you are not aware, Linux provides tab completion when you navigate through directories and Linux commands. You can
press the `TAB` key and Linux will try to auto complete the argument if possible. This can help save you time.

If you press `TAB` without any argument it will show all available commands available in Linux. 

```shell
root@f5fe1e2ad705:~# 
Display all 553 possibilities? (y or n)
```

If you are navigating through a directory structure and you press `TAB` it will show a list of directory or files
that match your input.

```shell
root@f5fe1e2ad705:~# ls -l /b
bin/  boot/ 
```

## Closing Remarks

We hope this tutorial helped you get familiar with Linux. To learn more about Linux we recommend
you check out [Intro to Linux by Linux Foundation](https://training.linuxfoundation.org/training/introduction-to-linux/).
